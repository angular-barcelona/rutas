import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'rutas-pantalla-pelis',
  templateUrl: './pantalla-pelis.component.html',
  styleUrls: ['./pantalla-pelis.component.css']
})
export class PantallaPelisComponent implements OnInit {

  public idPelicula = 6;

  constructor(private router: Router) { }

  public editarPelicula() {
    this.router.navigate(['/editar-pelicula', this.idPelicula], {
      queryParams: {
        pag: 2,
        order: 'fecha'
      }
    });
  }

  ngOnInit() {
  }

}
