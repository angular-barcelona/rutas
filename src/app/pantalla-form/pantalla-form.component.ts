import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'rutas-pantalla-form',
  templateUrl: './pantalla-form.component.html',
  styleUrls: ['./pantalla-form.component.css']
})
export class PantallaFormComponent implements OnInit {

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe(
      parametros => console.log('Parámetros fijos: ', parametros)
    );
    this.route.queryParamMap.subscribe(
      parametros => console.log('Parámetros variables:', parametros)
    );
    this.route.data.subscribe(
      parametros => console.log('Parámetros internos: ', parametros)
    );
  }

}
