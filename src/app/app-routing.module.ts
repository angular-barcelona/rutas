import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { PantallaPelisComponent } from './pantalla-pelis/pantalla-pelis.component';
import { HomeComponent } from './home/home.component';
import { PantallaFormComponent } from './pantalla-form/pantalla-form.component';
import { PantallaNotFoundComponent } from './pantalla-not-found/pantalla-not-found.component';

const rutas: Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'peliculas',
    component: PantallaPelisComponent
  },
  {
    path: 'nueva-pelicula',
    component: PantallaFormComponent,
    data: { accion: 'crear' }
  },
  {
    path: 'editar-pelicula/:id',
    component: PantallaFormComponent,
    data: { accion: 'editar' }
  },
  {
    path: 'resumen',
    loadChildren: () => import('./resumen/resumen.module').then(modulo => modulo.ResumenModule)
  },
  {
    path: '', redirectTo: '/home', pathMatch: 'full'
  },
  {
    path: '**', component: PantallaNotFoundComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(rutas)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
