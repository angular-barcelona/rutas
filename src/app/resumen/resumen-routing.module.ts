import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { PantallaEstadisticasComponent } from './pantalla-estadisticas/pantalla-estadisticas.component';

const rutas: Routes = [
  {
    path: '',
    component: PantallaEstadisticasComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(rutas)
  ]
})
export class ResumenRoutingModule { }
