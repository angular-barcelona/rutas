import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'rutas-pantalla-estadisticas',
  templateUrl: './pantalla-estadisticas.component.html',
  styleUrls: ['./pantalla-estadisticas.component.css']
})
export class PantallaEstadisticasComponent implements OnInit {

  constructor(private titleService: Title) { }

  ngOnInit() {
    this.titleService.setTitle('Resumen');
  }

}
