import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PantallaEstadisticasComponent } from './pantalla-estadisticas/pantalla-estadisticas.component';
import { ResumenRoutingModule } from './resumen-routing.module';

@NgModule({
  declarations: [PantallaEstadisticasComponent],
  imports: [
    CommonModule,
    ResumenRoutingModule
  ]
})
export class ResumenModule { }
