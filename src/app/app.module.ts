import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { PantallaPelisComponent } from './pantalla-pelis/pantalla-pelis.component';
import { PantallaFormComponent } from './pantalla-form/pantalla-form.component';
import { PantallaNotFoundComponent } from './pantalla-not-found/pantalla-not-found.component';
import { AppRoutingModule } from './app-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PantallaPelisComponent,
    PantallaFormComponent,
    PantallaNotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
