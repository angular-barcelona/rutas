import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'rutas-pantalla-not-found',
  templateUrl: './pantalla-not-found.component.html',
  styleUrls: ['./pantalla-not-found.component.css']
})
export class PantallaNotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
